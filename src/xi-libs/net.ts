export interface NetHandler {
    networkError(data: any);
    networkDone(data: any);
}
export class Net {
    static postJson(url: string, data: any, handler: NetHandler, timeout: number = 10000) {
        Net.post("application/json", url, data, handler, timeout);
    }
    static post(contentType: string, url: string, data: any, handler: NetHandler, timeout: number = 10000) {
        Net.send("post", contentType, url, data, handler, timeout);
    }
    static send(action: string, contentType: string, url: string, data: any, handler: NetHandler, timeout: number = 10000) {
        if (!url) {
            console.error("Missing URL");
        } else if (!handler) {
            console.error("Missing handler");
        } else if (!action) {
            console.error("Missing action");
        } else {
            let xhr = new XMLHttpRequest();

            xhr.open(action.toUpperCase(), url, true);
            if (contentType) {
                xhr.setRequestHeader("Content-Type", contentType);
            }
            xhr.timeout = timeout;

            xhr.onerror = e => {
                handler.networkError("Something went wrong (server nor reachable?)");
            };

            xhr.ontimeout = e => {
                handler.networkError("Got timeout");
            };

            xhr.onreadystatechange = () => {
                if (xhr.readyState == 4) {
                    let result = JSON.parse(xhr.responseText); // TODO dependent from content type!!

                    if (xhr.status == 200) {
                        handler.networkDone(result);
                    } else if (xhr.status == 400 && result.errors) {
                        let errors: string = "";

                        result.errors.forEach(error => {
                            errors += `
                    line:${error.locations[0].line},
                    column:${error.locations[0].column}\n\t
                    ${error.message}`;
                        });

                        handler.networkError(errors);
                    }
                }
            };

            xhr.send(
                JSON.stringify({ // TODO dependent from content type!!
                    operationName: "",
                    variables: {},
                    query: data
                })
            );
        }
    }
}