import { Vue } from "vue-property-decorator";
import { Net, NetHandler } from './net';

export class GraphQL implements NetHandler {

    serverURL: string;
    vue: Vue;

    constructor(serverURL: string, vue: Vue) {
        this.serverURL = serverURL;
        this.vue = vue;
    }

    networkError(data: any) {
    }

    networkDone(data: any) {
    }

    query() {
        //Net.postJson(this.serverURL, this.input.inputVal, this);
    }
}
