import Yaml from 'js-yaml';
import StreamZip from 'node-stream-zip';

const fs = require('fs');
const path = require('path');

class _Data {

    isLoaded: boolean;
    loader: _Loader;
    content: any;
    error: any;

    constructor(loader: _Loader) {
        this.isLoaded = false;
        this.loader = loader;
    }

    lines(handler: (data: any) => any, err?: (error: any) => any) {
        this._execute("text", data => {
            let lines = String(data).match(/[^\r\n]+/g);

            if (lines) {
                lines.forEach(line => handler(line));
            }
        }, err);
    }

    json(handler: (data: any) => any, err?: (error: any) => any) {
        this._execute("json", data => handler(JSON.parse(String(data))), err);
    }

    text(handler: (data: any) => any, err?: (error: any) => any) {
        this._execute("text", data => handler(String(data)), err);
    }

    object(handler: (data: any) => any, err?: (error: any) => any) {
        this._execute("object", data => handler(data), err);
    }

    yaml(handler: (data: any) => any, err?: (error: any) => any) {
        this._execute("yaml", data => handler(Yaml.load(String(data))), err);
    }

    _execute(type: string, handler: (data: any) => any, err?: (error: any) => any) {
        if (!this.isLoaded) {
            this.isLoaded = true;

            new Promise((resolve, reject) => {
                this.loader._load(type, resolve, reject);
            }).then(data => {
                handler(this.content = data);
            }).catch(error => {
                if (err) {
                    err(this.error = error);
                } else {
                    this.error = error;
                }
            });
        } else {
            if (this.error) {
                if (err) {
                    err(this.error);
                }
            } else {
                handler(this.content);
            }
        }
    }
}

interface _Loader {
    _load(type: string, resolve, reject);
}

class _File implements _Loader {

    as: _Data;
    utf: string | undefined;
    filePath: string;

    constructor(filePath: string, utf?: string) {
        this.filePath = filePath;
        this.utf = utf;
        this.as = new _Data(this);
    }

    _load(type: string, resolve, reject) {
        if (this.filePath) {
            if (this.utf) {
                fs.readFile(path.resolve(this.filePath), this.utf, (error, data) => error ? reject(error) : resolve(data));
            } else {
                fs.readFile(path.resolve(this.filePath), (error, data) => error ? reject(error) : resolve(data));
            }
        }
    }
}

class _Net implements _Loader {

    as: _Data;
    xhr: XMLHttpRequest;
    action: string;
    resolve: any;
    reject: any;

    constructor(url: string, action: string) {
        this.xhr = new XMLHttpRequest();
        this.action = action;
        this.as = new _Data(this);

        // @PUT && @DELETE url might need parameter/path-segments to point as specific entry to modify/delete, like url+"/12"
        this.xhr.open(this.action.toUpperCase(), url, true/*TBD ASYNC?*/);

        this.xhr.onerror = this._handleError;
        this.xhr.ontimeout = this._handleError;
        this.xhr.onload = this._handleResponse.bind(this);
    }

    _load(type: string, resolve, reject) {
        this.resolve = resolve;
        this.reject = reject;

        switch (this.action) {
            case "GET":
                this.xhr.send(null);
                break;
            case "PUT":
                this._setContentType(type);
                this.xhr.send(JSON.stringify({ firstname: "John", lastname: "Snow" }));
                break;
            case "POST":
                this._setContentType(type);
                this.xhr.send(JSON.stringify({ firstname: "John", lastname: "Snow" }));
                break;
            case "DELETE":
                this.xhr.send(null);
                break;
        }
    }

    _setContentType(type: string) {
        switch (type.toLowerCase().trim()) {
            case "json":
                this.xhr.setRequestHeader("Content-type", "application/json; charset=utf-8"); // TBD define charset?
                break;
        }
    }

    _handleError(e) {
        this.reject(e);
    }

    _handleResponse(e) {
        if (this.xhr.readyState && this.xhr.readyState === 4) {
            if (this.xhr.status == 200) {
                this.resolve(this.xhr.responseText);
            } else {
                this.reject("Error " + this.xhr.status);
            }
        }
    }
}

export class Zip {

    zipPath;
    entryPath;
    zipStream;

    // TODO if no callback maybe don't close, instead read an keep index so Zip-Instance can be used further on
    constructor(zipURL, callback?) {
        if (zipURL) {
            if (zipURL.indexOf('%') != -1) {
                zipURL = decodeURI(zipURL);
            }

            const splitIndex = zipURL.indexOf('!/');

            if (splitIndex > -1) {
                this.zipPath = zipURL.substring(0, splitIndex);
                this.entryPath = zipURL.substring(splitIndex + 2);
                this.zipStream = new StreamZip({ file: this.zipPath, storeEntries: true/*TODO*/ });
                this.zipStream.on('ready', () => {
                    const data = this.zipStream.entryDataSync(this.entryPath);

                    if (callback) {
                        callback({ data: data, mimeType: "image" });
                    } else {
                        console.log('no callback, read index');
                    }

                    this.zipStream.close();
                });
            } else {
                this.zipPath = zipURL;
                this.zipStream = new StreamZip({ file: zipURL, storeEntries: true/*TODO*/ });
                this.zipStream.on('ready', () => {
                    if (callback) {
                        callback(this);
                    }

                    this.zipStream.close();
                });
            }
        } else {
            console.error('Missing zip-URL'); // TODO throw new Exception();
        }
    }

    get length() {
        return this.zipStream ? this.zipStream.entriesCount : -1;
    }

    forEach(callback) {
        if (callback && this.zipStream) {
            for (const zipEntry of Object.values(this.zipStream.entries())) {
                callback(zipEntry);
            }
        }
    }

    getAsURL(zipEntry) {
        return this.zipPath ? encodeURI('zip://' + this.zipPath + '!/' + zipEntry['name']) : null;
    }

    getAsImage(zipEntry) {
        return this.zipStream
            ? new Blob([this.zipStream.entryDataSync(zipEntry['name'])], { type: "image/jpeg" }) // TODO get image type?
            : null;
    }
}


export class Get {
    static file: any = (filePath: string, utf?: string) => new _File(filePath, utf);
    static url: any = (url: string) => new _Net(url, "GET");
}
export class Post { // similar to create & write file?
    //static file: any = (filePath: string, utf?: string) => new _File(filePath, utf);
    static url: any = (url: string) => new _Net(url, "POST");
}
export class Put { // similar to write file?
    //static file: any = (filePath: string, utf?: string) => new _File(filePath, utf);
    static url: any = (url: string) => new _Net(url, "PUT");
}
export class Delete {
    static file: any = (filePath: string, utf?: string) => new _File(filePath, utf);
    static url: any = (url: string) => new _Net(url, "DELETE");
}
export class Read {
    static file: any = (filePath: string, utf?: string) => new _File(filePath, utf);
    static url: any = (url: string, action: string) => new _Net(url, action);
}
export class Write {
    // TODO similar to Post?
}
