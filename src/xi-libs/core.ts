import Vue from 'vue';
import Xi from './xi';
import JQuery from 'jquery';

export const xi = Xi;
export const $ = JQuery; // make jquery available

// note: jquery-plugins won't work right away, better to wrap them into an exported function where
// you provide $/jQuery as parameter. So all plugins must be loaded after we defined '$' above

// import Turn.js
import TurnJs from '../../static/js/turn.js';
// init Turn.js
TurnJs($,$);







// --------------------

export const EventBus = new Vue();

export const $fire = (scope: any, type: string, action: any, target: any, ...params: Array<any>) => {
    EventBus.$emit(type, new Event(scope, type, action, target, params));
};

export const $on = (type: string, handler) => {
    EventBus.$on(type, handler);
};

export const $ancestor = (vue: Vue, type: string) => {
    while (vue && type) {
        if (vue.constructor && type === vue.constructor.name) {
            return vue;
        }
        vue = vue.$parent;
    }
    return null;
};

export class Styles {
    static getStyle(cssSelector: string): Style {
        let style: Style = new Style();

        if (cssSelector) {
            Array //
                .from(document.styleSheets)
                .forEach(styleSheet => {
                    if (styleSheet) {
                        let cssStyleSheet: CSSStyleSheet = styleSheet as CSSStyleSheet;

                        try {
                            if (cssStyleSheet.cssRules) {
                                Array //
                                    .from(cssStyleSheet.cssRules)
                                    .forEach(rule => {
                                        let cssRule: CSSStyleRule = rule as CSSStyleRule;

                                        if (cssRule.selectorText === cssSelector/*".theme--dark"*/) {
                                            style.css = cssRule.style;
                                            return;
                                        }
                                    });
                            }
                        } catch (error) { }
                    }
                });
        }

        return style;
    }
}

class Event {
    scope: any;
    type: string;
    action: any;
    target: any;
    params: Array<any>;

    constructor(scope: any, type: string, action: any, target: any, ...params: Array<any>) {
        this.scope = scope;
        this.type = type;
        this.action = action;
        this.target = target;
        this.params = params;
    }
}

class Style {

    css!: CSSStyleDeclaration;

    get(key: string) {
        return this.css ? this.css.getPropertyValue(key) : undefined;
    }

    set(key: string, value: string) {
        if (this.css && key && value) {
            this.css.setProperty(key, value);
        }
    }
}

export class Is {
    static present(obj: any): boolean {
        if (Array.isArray(obj)) {
            return obj.length > 0;
        } else {
            return obj && (typeof obj === 'string') && obj.length > 0;
        }
    }

    static empty(o: any): boolean {
        return o === null || o === undefined || (typeof o === 'string' && o.length == 0);
    }
}


export class As {

    static REPLACER: RegExp = /\{\{([^\}]*)\}\}/g;

    static repeated(str: string, amount: number): string {
        let rStr = '';
        if (str && amount) {
            for (let i = 0; i < amount; i++) {
                rStr += str;
            }
            rStr = rStr.trim();
        }
        return rStr;
    }

    static title(str: string): string {
        let words: Array<string> = As.splitted(str.replace(/_/g, ' '), ' ', true);

        words = words.map(word => {
            return As.capitalize(word);
        });

        return (str.charAt(0) === '_' ? '_' : '') + words.join(' ');
    }

    static capitalize(str: string): string {
        return str.charAt(0).toUpperCase() + str.slice(1);
    }

    static capitalizeAll(str: string): string {
        return str.split(' ').map(s => {
            return s.charAt(0).toUpperCase() + s.slice(1);
        }).join(' ');

//        return str.charAt(0).toUpperCase() + str.slice(1);
    }

    static replace(pattern: string, params: Array<any>): string {
        return As.parsed(pattern, As.REPLACER, (result, groupIndex) => {
            let index: number = parseInt(String(result.get(1)));

            return index >= 0 && index < params.length ? String(params[index]) : "";
        });
    }

    static splitted(str: string, regex: RegExp | string, trimmed: boolean): Array<string> {
        if (regex === '|') {
            regex = '\|';
        }

        let arr: Array<string> | null = str != null && regex != null ? str.split(regex) : null;

        if (arr == null) {
            arr = [str];
        }
        if (trimmed) {
            for (let i = 0; i < arr.length; i++) {
                arr[i] = arr[i] != null ? arr[i].trim() : arr[i];
            }
        }

        return arr;
    }

    static parsed(s: string, p: RegExp, callback: (RegexResult, number) => string): string {
        return As.parsedImpl(s, As.completeRegexResultList(s, p), callback);
    }

    static parsedImpl(s: string, results: Array<RegexResult>, callback: (RegexResult, number) => string): string {
        if (s && results && callback) {
            let b: StringBuilder = new StringBuilder(s);

            for (let i = results.length - 1; i >= 0; i--) {
                let match: RegexResult = results[i];

                b.delete(match.start, match.end);
                b.insert(match.start, callback.call(this, match, i));
            }
            s = b.toString();
        }

        return s;
    }

    static completeRegexResultList(s: string, p: RegExp): Array<RegexResult> {
        if (p.constructor !== RegExp) {
            throw new Error('not RegExp');
        }

        let res: Array<RegexResult> = [];
        let match: RegExpExecArray | null = null;

        if (p.global) {
            while (match = p.exec(s)) {
                res.push(new RegexResult(match));
            }
        } else {
            if (match = p.exec(s)) {
                res.push(new RegexResult(match));
            }
        }

        return res;
    }
}

export class Timer {

    counter: number;
    handle: any | undefined;
    monitor: any;
    vars: Array<any>;
    varDelta: Array<any>;

    constructor(monitor: any, ...vars) {
        this.counter = 10;
        this.monitor = monitor;
        this.vars = vars;
        this.varDelta = [];

        this.vars.forEach(v => {
            this.varDelta.push(v / this.counter);
        });
    }

    get(index) {
        return index >= 0 && index < this.vars.length ? this.vars[index] : 0;
    }

    start() {
        this.handle = setInterval(this.update.bind(this), 25);
    }

    update() {
        if (this.counter > 0) {
            for (let i = 0; i < this.vars.length; i++) {
                this.vars[i] = this.varDelta[i] * this.counter;
            }
            this.counter--;
            if (this.counter == 0 && this.handle) {
                clearTimeout(this.handle);

                this.handle = null;
                if (this.monitor) {
                    this.monitor('end');
                }
            } else {
                this.monitor('update');
            }
        }
    }
}

export class Color {

    static RGBA_PATTERN: RegExp = /^rgba?\W?\(\W?(\d+)\W?,\W?(\d+)\W?,\W?(\d+)(\W?,\W?((\d+)?\.?\d+))?\W?\)$/;

    r: number = 0;
    g: number = 0;
    b: number = 0;
    a: number = 0;

    constructor(colorStr: string) {
        if (colorStr.length > 0 && colorStr.charAt(0) === '#') {
            this._fromHex(colorStr);
        } else if (colorStr.length > 3 && colorStr.startsWith('rgb')) {
            this._fromRgba(colorStr);
        } else {
            console.error('Cannot convert color from \'' + colorStr + '\'');
        }
    }

    get hex(): string {
        return `#${this.toHex(this.r)}${this.toHex(this.g)}${this.toHex(this.b)}`;
    }
    get rgba(): string {
        return `rgba(${this.r}, ${this.g}, ${this.b}, ${this.a})`;
    }

    compare(color: any, includeAlpha: boolean = true): boolean {
        if (typeof color === 'string') {
            color = new Color(color);
        }
        if (color) {
            return this.r === color.r &&
                this.g === color.g &&
                this.b === color.b &&
                (!includeAlpha || (includeAlpha && this.a === color.a));
        }

        return false;
    }

    toHex(value: number): string {
        let hex = value.toString(16);
        if (hex.length == 1) {
            hex = '0' + hex;
        }
        return hex;
    }

    _fromHex(colorStr: string): void {
        if (colorStr.length === 4) {
            this.r = parseInt(colorStr.charAt(1) + colorStr.charAt(1), 16);
            this.g = parseInt(colorStr.charAt(2) + colorStr.charAt(2), 16);
            this.b = parseInt(colorStr.charAt(3) + colorStr.charAt(3), 16);
        } else if (colorStr.length === 7) {
            this.r = parseInt(colorStr.substr(1, 3), 16);
            this.g = parseInt(colorStr.substr(3, 5), 16);
            this.b = parseInt(colorStr.substr(5, 7), 16);
        }
        this.a = 1;
    }

    _fromRgba(colorStr: string): void {
        let matches: RegExpExecArray | null = Color.RGBA_PATTERN.exec(colorStr);

        if (matches) {
            this.r = parseInt(matches[1]);
            this.g = parseInt(matches[2]);
            this.b = parseInt(matches[3]);
            this.a = matches[5] ? parseFloat(matches[5]) : 1;
        }
    }
}

Color.prototype.toString = function () {
    return `Color[r:${this.r},g:${this.g},b:${this.b},a:${this.a}]`;
};

export interface Map<K, V> {
    put(key: K, value: V);
    get(key: K): V;
    keySet(): List<K>;
    containsKey(key: K): boolean
}

export interface List<T> {
    add(entry: T);
    addAll(collection: List<any>);
    size(): number;
    get(index: number): T | null;
    forEach(callback);
    sort();
}

export class Collections {
    static sort(collection: List<any>) {
        collection.sort();
    }
}

export class HashMap<K, V> implements Map<K, V> {

    data: Object;

    constructor() {
        this.data = {};
    }

    put(key: any, value: any) {
        this.data[key] = value;
    }
    get(key: any): V {
        return this.data[key];
    }
    keySet(): List<K> {
        let keys: List<any> = new ArrayList<K>();

        Object.keys(this.data).forEach(key => keys.add(key));

        return keys;
    }
    containsKey(key: K): boolean {
        return this.data.hasOwnProperty(String(key));
    }
}

export class ArrayList<T> implements List<T> {

    data: Array<T>;

    constructor(initialSize?: number) {
        this.data = initialSize ? new Array(initialSize) : new Array();
    }

    add(entry: T) {
        this.data.push(entry);
    }

    addAll(collection: List<any>) {
        for (let i = 0; collection && i < collection.size(); i++) {
            this.data.push(collection.get(i));
        }
    }

    size(): number {
        return this.data.length;
    }

    get(index: number): T | null {
        return index >= 0 && index < this.data.length ? this.data[index] : null;
    }

    forEach(callback) {
        this.data.forEach(entry => callback(entry));
    }

    sort() {
        this.data.sort();
    }
}

export class Pattern {
    static CASE_INSENSITIVE: 1;

    static compile(regex: RegExp): Pattern {
        return new Pattern(regex);
    }

    regex: RegExp;

    constructor(regex: RegExp) {
        this.regex = regex;
    }

    matcher(str: string): Matcher {
        return new Matcher(this, str);
    }
}

export class Matcher {

    pattern: Pattern;
    matches: Array<RegExpExecArray>;
    pointer: number;
    currentMatch: any;
    str: string;

    constructor(pattern: Pattern, str: string) {
        this.pattern = pattern;
        this.matches = [];
        this.pointer = 0;
        this.str = str;

        this._exec(str);
    }

    _exec(str: string) {
        let match: RegExpExecArray | null = null;

        this.matches = new Array();
        this.pointer = 0;

        while (match = this.pattern.regex.exec(str)) {
            this.matches.push(match);

            if (!this.pattern.regex.global) {
                break;
            }
        }
    }

    start(): number {
        return this.currentMatch && this.currentMatch.length > 0 ? this.currentMatch.index : 0;
    }

    end(): number {
        return this.currentMatch && this.currentMatch.length > 0 ? this.currentMatch.index + this.currentMatch[0].length : 0;
    }

    find(): boolean {
        let hasMatch = this.pointer < this.matches.length;

        this.currentMatch = hasMatch ? this.matches[this.pointer] : null;
        this.pointer++;
        return hasMatch;
    }

    reset(str: string): Matcher {
        this._exec(this.str = str);
        return this;
    }

    group(index?: number): string {
        if (this.currentMatch) {
            if (index) {
                return this.currentMatch[index];
            } else {
                return this.currentMatch[0];
            }
        }
        return '';
    }

    groupCount(): number {
        return this.currentMatch ? this.currentMatch.length : 0;
    }

    replaceAll(replacement: string): string {
        return this._replace(replacement, -1);
    }

    replaceFirst(replacement: string): string {
        return this._replace(replacement, 0);
    }

    _replace(replacement: string, stopAtIndex: number): string {
        let offset: number = 0;
        let rtIndex: number = 0;
        let modified: string = this.str;

        this.matches.forEach(match => {
            if (stopAtIndex === -1 || rtIndex <= stopAtIndex) {
                let index = match.index + offset;
                let length = match[0].length;

                modified = modified.slice(0, index)
                    + replacement
                    + modified.slice(index + length, modified.length);

                offset += replacement.length - length;
            }
            rtIndex++;
        });
        return modified;
    }
}

export class Random {

    constructor() {
    }

    nextInt(range: number): number {
        return Math.trunc(range * Math.random());
    }

    nextDouble(): number {
        return Math.random();
    }
}

export class char {

    code: number;
    c: string;

    constructor(c: string | number) {
        if (typeof c === 'string') {
            this.c = c;
            this.code = this.c.charCodeAt(0);
        } else {
            this.code = c;
            this.c = String.fromCharCode(this.code);
        }
    }

    equals(other: any): boolean {
        if (typeof other === 'string') {
            return (other as string) === this.c;
        } else if (typeof other === 'number') {
            return (other as number) === this.code;
        }
        return false;
    }
}

char.prototype.toString = function () {
    return this.c;
}

export class StringBuilder {

    str: string;

    constructor(str?: string) {
        this.str = str ? String(str) : '';
    }

    deleteCharAt(index: number): StringBuilder {
        this.str = this.str.slice(0, index) + this.str.slice(index + 1, this.str.length);
        return this;
    }
    setLength(length: number): StringBuilder {
        this.str = this.str.slice(0, length);
        return this;
    }

    charAt(index: number): number {
        return this.str.charCodeAt(index);
    }

    setCharAt(index: number, insertion: number | string): StringBuilder {
        let c: string;

        if (typeof insertion === 'string') {
            c = insertion as string;
        } else {
            c = String.fromCharCode(insertion as number);
        }

        this.str = this.str.substr(0, index) + c + this.str.substr(index + 1);
        return this;
    }

    length(): number {
        return this.str.length;
    }

    delete(start: number, end: number): StringBuilder {
        this.str = this.str.slice(0, start) + this.str.slice(end, this.str.length);
        return this;
    }

    insert(start: number, inserted: string): StringBuilder {
        this.str = this.str.slice(0, start) + String(inserted) + this.str.slice(start, this.str.length);
        return this;
    }

    append(str: any) {
        this.str += String(str);
        return this;
    }

    replace(start: number, end: number, replacement: string): StringBuilder {
        this.str = this.str.slice(0, start) + replacement + this.str.slice(end, this.str.length);
        return this;
    }
}

StringBuilder.prototype.toString = function () {
    return this.str;
}

export class Str {
    static format(pattern: string, ...params): string {
        return ''; // TODO
    }

    static contains(str: string, contained: string): boolean {
        return str.indexOf(contained) != -1;
    }

    static replaceAll(str: string, before: string, after: string): string {
        var target = str;
        return target.split(before).join(after);
    }

    static equalsIgnoreCase(strA: string, strB: string): boolean {
        return strA.toUpperCase() === strB.toUpperCase();
    }
    static equals(strA: any, strB: any): boolean {
        return strA === strB;
    }
}

class RegexResult {

    start: number;
    end: number;
    fragment: Array<string>;

    constructor(matcher: RegExpExecArray) {
        this.start = matcher.index;
        this.end = matcher.index + matcher[0].length;
        this.fragment = new Array();

        for (let i = 0; i < matcher.length; i++) {
            this.fragment.push(matcher[i]);
        }
    }

    get(index: number): string | null {
        return index >= 0 && index < this.fragment.length ? this.fragment[index] : null;
    }
}
