class Xi {
    // This contains all elements that should be manipulating
    nodes: Array<HTMLElement> = [];

    static $(...elementsOrSelectors: Array<any>) {
        return new Xi(elementsOrSelectors);
    }

    static snakeToCamelCase(str: string): string {
        if (str) {
            str = str.split('-').map(s => Xi.capitalize(s)).join('');
            str = str.charAt(0).toLowerCase() + str.slice(1);
        }
        return str;
    }

    static capitalize(str: string): string {
        if (str && str.length > 0) {
            str = str.charAt(0).toUpperCase() + str.slice(1);
        }
        return str;
    }

    /**
     * Wraps some object so we can modify it. 'elementsOrSelectors' might be a
     * query-selector-string or a HTMLElement (or inheriting from it) or a Vue-Instance.
     * 
     * @param elementsOrSelectors string, HTMLElement or Vue
     * @param elementRoot (optional) a root from where we do our query (only effective when used in combination with a query-selector-string)
     */
    constructor(elementsOrSelectors: Array<any>, elementRoot: any | undefined = undefined) {
        elementsOrSelectors.forEach(elementOrSelector => {
            if (typeof elementOrSelector === 'string') {
                elementRoot = elementRoot || document; // if no root is set we rely on 'document

                // turn given 'elementRoot' into something HTMLElement-like
                // and iterate over it, triggering a query on each. The result
                // of each iteration will be pushed into our 'nodes'-array
                this._convertToElements(elementRoot).forEach(elm => {
                    Array //
                        .from(elm.querySelectorAll(elementOrSelector))
                        .map(result => result as HTMLElement)
                        .forEach(result => this.nodes.push(result));
                });
            } else {
                // the element to be wrapped is not a query, so try to convert
                // it into something usable
                this._convertToElements(elementOrSelector).forEach(elm => this.nodes.push(elm));
            }
        });
    }

    /**
     * This method takes some HTML-formated string to convert it into HTMLElement(s).
     * 
     * @param html a string with HTML-formated content
     * @returns Node | null
     */
    htmlToElement(html: string): Node|null {
        if (html) {
            let template: HTMLTemplateElement = document.createElement('template');
            template.innerHTML = html.trim();
            return template.content.firstChild;
        }
        return null;
    }

    /**
     * Takes a HTML-formated string, turns it into a HTML-Element and wraps all internally
     * stored elements into this generated HTML-Element.
     * 
     * @param html a string with HTML-formated content
     * @returns this
     */
    wrap(html: string): Xi {
        if (html) {
            this.nodes.forEach(node => {
                if (node) {
                    let parent: HTMLElement|null = node.parentElement;

                    if (parent) {
                        let wrapper = this.htmlToElement(html);

                        if (wrapper) {
                            parent.removeChild(node);
                            this._deepestChild(wrapper).appendChild(node);
                            parent.appendChild(wrapper as HTMLElement);
                        }
                    }
                }
            });
        }
        return this;
    };

    /**
     * Takes either a single string-key and value parameter or a single object-parameter with
     * key-values to define css-values on all internally wrapped elements.
     * If a string-key is used it'll be automatically converted from snake- to camelcase type,
     * so both ways are allowed for CSS-keys.
     * 
     * @param keyOrObject a single string-key or object
     * @param value (optional) a value (if a string-key was used)
     * @returns this
     */
    css(keyOrObject: any, value: any = undefined): Xi {
        if (typeof keyOrObject === 'string') {
            if (value) {
                this.nodes.forEach(elm => elm.style[keyOrObject] = value);
            } else {
                return this.nodes.length > 0 ? this.nodes[0].style[Xi.snakeToCamelCase(keyOrObject)] : undefined;
            }
        } else {
            this.nodes.forEach(elm =>
                Object.keys(keyOrObject).forEach(key =>
                    elm.style[key] = keyOrObject[key]
                )
            );
        }
        return this;
    }

    /**
     * Modifies one or more CSS-Classnames on all internally wrapped elements.
     * If more then one class should be modified they should be separated by a space.
     * If the plain classname is used, it will be added. Otherwise you can use
     * single-characters directly on front of each classname to indicate your action:
     * '+' : the classname should be added, like '+myCssClass'
     * '-' : the classname should be removed, like '-myCssClass'
     * '~' : the classname should be toggled, like '~myCssClass'
     * A more complex example could be like this:
     *    '+foo -bar ~ipsum lorem'
     *    Would result in:
     *    add foo
     *    remove bar
     *    toggle ipsum
     *    add lorem
     * 
     * @param cssClassNames the class name(s) to be modified
     */
    class(cssClassNames: string): Xi {
        if (cssClassNames) {
            let cssClassNameSeq: string[] = cssClassNames.split(' ');

            this.nodes.forEach(elm => {
                cssClassNameSeq.forEach(cssClassName => {
                    let cropped: string = cssClassName.slice(1);
                    switch (cssClassName.charAt(0)) {
                        case '-':
                            elm.classList.remove(cropped);
                            break;
                        case '~':
                            elm.classList.toggle(cropped);
                            break;
                        case '+':
                            elm.classList.add(cropped);
                            break;
                        default:
                            elm.classList.add(cssClassName);
                            break;
                    }
                });
            });
        }
        return this;
    }

    /**
     * Applies a click-handler for all internally stored elements
     * 
     * @param handler 
     */
    click(handler): Xi {
        handler && this.nodes.forEach(elm => elm.addEventListener('click', handler));
        return this;
    }

    /**
     * Helper-method to convert an 'obj' into something HTMLElement-like, if possible. In some
     * cases it could be a list of elements, so this method returns an array of elements, this
     * be of length === 0 if 'obj' wasn't convertibale, length === 1 in most cases and legth > 1
     * if 'obj' was already some sort of NodeList or similar.
     * 
     * @param obj object to convert
     */
    private _convertToElements(obj: any): Array<HTMLElement> {
        let result: Array<HTMLElement> = [];

        if (obj && obj.$el) {
            result.push(obj.$el);
        } else {
            if ('NodeList' === obj.constructor.name) {
                Array.from(obj).forEach(node => {
                    result.push(node as HTMLElement);
                });
            } else {
                result.push(obj);
            }
        }
        return result;
    }

    private _deepestChild(node) {
        while (node && node.firstChild) node = node.firstChild;
        return node;
    }
}



export default Xi;