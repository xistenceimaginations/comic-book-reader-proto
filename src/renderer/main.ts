import Vue from 'vue';
import App from './App.vue';

if (!process.env.IS_WEB) Vue.use(require('vue-electron'))

process.env['ELECTRON_DISABLE_SECURITY_WARNINGS'] = 'true';

Vue.config.productionTip = false

/* eslint-disable no-new */
new Vue({
  render: h => h(App)
}).$mount('#app')
