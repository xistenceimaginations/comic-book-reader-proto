'use strict';

import { app, BrowserWindow } from 'electron';
import { protocol } from 'electron';
import StreamZip from 'node-stream-zip';
import { Zip } from '../xi-libs/io';

/**
 * Set `__static` path to static files in production
 * https://simulatedgreg.gitbooks.io/electron-vue/content/en/using-static-assets.html
 */
if (process.env.NODE_ENV !== 'development') {
  global.__static = require('path').join(__dirname, '/static').replace(/\\/g, '\\\\');
}

let mainWindow;
const winURL = process.env.NODE_ENV === 'development'
  ? `http://localhost:9080`
  : `file://${__dirname}/index.html`;

function createWindow() {

  // register protocol to access zip content directly via URL in HTML (example: 'zip://static/test.zip!/image_l1.jpg').
  // note that 'test.zip' has to be in 'static' and that everything after 'test.zip/' is ment to be the internal
  // zip-path
  protocol.registerBufferProtocol("zip", async (request, callback) => {
    await new Zip(request.url.substring(6), callback);
  });


  /**
   * Initial window options
   */
  mainWindow = new BrowserWindow({
    height: 563,
    useContentSize: true,
    width: 1000,
    frame: false, // no titlebar, etc.
    webPreferences: {
      blinkFeatures: 'OverlayScrollbars',
      webSecurity: false
    }
  });

  mainWindow.loadURL(winURL);

  mainWindow.on('closed', () => {
    mainWindow = null;
  });
}

app.on('ready', createWindow);

app.on('window-all-closed', () => {
  if (process.platform !== 'darwin') {
    app.quit();
  }
});

app.on('activate', () => {
  if (mainWindow === null) {
    createWindow();
  }
});

// to prevent the default frame menu (if not frameless, s. above)
app.on('browser-window-created', (e, window) => {
  window.setMenu(null);
});
