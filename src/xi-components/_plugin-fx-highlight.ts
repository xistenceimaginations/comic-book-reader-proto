import { Vue } from "vue-property-decorator";

const FxHighlight = {
    install(Vue, options) {
        Vue.mixin({
            mounted() {
                if (this.$el.getAttribute('fx-highlight') !== undefined &&
                    this.$el.getAttribute('fx-highlight') !== null) {
                    (this.$fx = this.$fx || []).push(new FxHighlightEffect(this));
                }
            }
        });
    }
}

class FxHighlightEffect {
    highlightContainer;
    elm;
    inDebounce;
    rippler;

    constructor(vue) {
        this.elm = vue.$el;
        this.highlightContainer = document.createElement('div');
        this.highlightContainer.className = 'fx-highlight--container';

        this.rippler = document.createElement('span');
        this.highlightContainer.appendChild(this.rippler);

        this.elm.addEventListener('mousemove', e => this.showRipple(e));
        this.elm.addEventListener('mouseout', e=> this.debounce());
        this.elm.appendChild(this.highlightContainer);
    }

    showRipple(e) {
        let size = this.elm.offsetWidth;
        let pos = this.elm.getBoundingClientRect();

        this.rippler.style.opacity = '1';

        size = 128;
        return this.rippler.setAttribute('style',
            'top:' + (e.pageY - pos.top - (size / 2)) + 'px;' +
            'left: ' + (e.pageX - pos.left - (size / 2)) + 'px;' +
            'height: ' + size + 'px;' +
            'width: ' + size + 'px;'
        );
    }

    debounce() {
        this.rippler.style.opacity = '0';
    }
}

export default FxHighlight;