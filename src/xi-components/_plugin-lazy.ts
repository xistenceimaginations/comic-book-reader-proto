import { Vue } from "vue-property-decorator";

const Lazy = {
    install(Vue, options) {
        Vue.mixin({
            mounted() {
                if (this.$attrs.lazy !== undefined) {
                    let direction: string = this.$attrs.lazy.trim().length == 0 ? 'bottom' : this.$attrs.lazy.trim();
                    let vue: Vue = this;

                    while (vue) {
                        if (vue.$el.classList.contains("xScrollPane")) {
                            break;
                        }
                        vue = vue.$parent;
                    }
                    if (vue != null) {
                        let el = this.$el;

                        this.$el.style.opacity = 0;
                        switch (direction) {
                            case 'top':
                                this.$el.style.transform = "translateY(-50px)";
                                break;
                            case 'left':
                                this.$el.style.transform = "translateX(-50px)";
                                break;
                            case 'right':
                                this.$el.style.transform = "translateX(50px)";
                                break;
                            case 'bottom':
                            default:
                                this.$el.style.transform = "translateY(50px)";
                                break;
                        }
                        //this.$el.style.transform = "translateY(50px)";
                        this.$el.style.transition = (this.$el.style.transition ? this.$el.style.transition + ', ' : '') + "transform .6s";

                        new IntersectionObserver(
                            (entries, observer) => {
                                entries.forEach(entry => {
                                    if (entry.isIntersecting) {
                                        el.style.opacity = "1";
                                        el.style.transform = "translate(0, 0)";
                                    }
                                });
                            },
                            {
                                root: vue.$el,
                                rootMargin: "0px",
                                threshold: 0.1 // [0.0, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1.0] // how much of the element must be visible before callback is triggered (percent)
                            }
                        ).observe(this.$el);
                    }
                }
            }
        });
    }
}

export default Lazy;