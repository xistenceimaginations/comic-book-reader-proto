import { Vue } from "vue-property-decorator";

const FxRipple = {
    install(Vue, options) {
        Vue.mixin({
            mounted() {
                if (this.$el.getAttribute('fx-ripple') !== undefined &&
                    this.$el.getAttribute('fx-ripple') !== null) {
                    (this.$fx = this.$fx || []).push(new FxRippleEffect(this));
                }
            }
        });
    }
}

class FxRippleEffect {
    rippleContainer;
    elm;
    inDebounce;

    constructor(vue) {
        this.elm = vue.$el;
        this.rippleContainer = document.createElement('div');
        this.rippleContainer.className = 'fx-ripple--container';

        this.elm.addEventListener('mousedown', e => this.showRipple(e));
        this.elm.addEventListener('mouseup', this.debounce());
        this.elm.appendChild(this.rippleContainer);
    }

    showRipple(e) {
        let rippler = document.createElement('span');
        let size = this.elm.offsetWidth;
        let pos = this.elm.getBoundingClientRect();
        this.rippleContainer.appendChild(rippler);

        return rippler.setAttribute('style',
            'top:' + (e.pageY - pos.top - (size / 2)) + 'px;' +
            'left: ' + (e.pageX - pos.left - (size / 2)) + 'px;' +
            'height: ' + size + 'px;' +
            'width: ' + size + 'px;'
        );
    }

    debounce() {
        let inDebounce;
        let scope = this;

        return function () {
            clearTimeout(inDebounce);
            return inDebounce = setTimeout(function () {
                while (scope.rippleContainer.firstChild) {
                    scope.rippleContainer.removeChild(scope.rippleContainer.firstChild);
                }
            }, 2000);
        };
    }
}

export default FxRipple;