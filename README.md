# comic-book-reader

> This is experimental code, so nothing to be used as library or as code to rely on. All code is subject to change and should be used as example how some things can be done or were solved!

#### What it is

> A prototype for a comic book reader (reading images packed in ZIP/CBZ or anything that is based on ZIP). It is based on a Electron/Vue/Typescript/Pug/Sass-construction and used [turn.js](http://www.turnjs.com/) for the page animation.

#### Important Notes

Note that this is a prototype. It is presented as it is, so there is no guarantee of being feature complete and/or bug free. Also note that i might not accept feature- or bug-request (although suggestions and bug-reports are still welcome) because of limitation of time. You can fork and modify this code as long you respect the license either on this code itself or third-party code (s. turn.js).

#### Build Setup

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:9080
npm run dev

# build electron application for production
npm run build


# lint all JS/Vue component files in `src/`
npm run lint

```

#### Usage

You can use this application as you either build it and run the proper executable or by executing
```
npm run dev
```
on the commandline

#### Notes

In case you're going to work with this code, note that some part of this code (mainly the libraries and all xi-components) are based on other projects which aren't separated as single modules yet. Changing that happens on your own risc as these code parts are subject to change.
Another important thing is about the aspect ratio: for now there is a fix ratio in a DIN A format (portrait). There is no explicite code that is checking the ratio of all pages to find the best (but it is progress).

#### Used third-party code

[turn.js](http://www.turnjs.com/) for the page-flip animation
[Fontawesome](http://fontawesome.io/) for the icons (in its free-version)

#### License

The code written in this project is set under the [MIT-License](https://choosealicense.com/licenses/mit/) while all other code and/or libraries are each still under their own explicite license, please inform yourself following the provided links above ('Used third-party code').